%define next_pointer %%next_pointer


%macro colon 2
    
    %ifid %%next_pointer
        %%label: dq next_pointer
        %2: %define next_pointer %%next_pointer 
    %else
        %fatal "Ожидался идентификатор"
    %endif

    %ifstr %1
        db %1, 0
    %else
        %fatal "Ожидалась строка как значение ключа"
    %endif

%endmacro
