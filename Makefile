ASM        = nasm
ASMFLAGS   =-felf64 -g

main: main.o lib.o dict.o
	ld -o $@ $^

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

clean: 
	rm main main.o dict.o lib.o

.PHONY: clean

