%define DQ_SIZE 8

global find_word
extern string_equals

                            ; rsi - начало словаря
                            ; rdi - указатель на нуль-терминированную строку
                            ; Возвращает в rax метку совпавшей строки, иначе 0
find_word:

    .loop:
        push rsi            ; Сохраняем в стек, так как моя замечательная реализация string_equals
        push rdi            ; вместо того чтобы использовать rcx для итерации по строкам, инкрементит 
        add rsi, DQ_SIZE    ; rsi и rdi. Смещаем указатель на ключ
        call string_equals  ; Сравниваем значение ключа и строки
        pop rdi             ; Восстанавливаем rdi
        pop rsi             ; Восстанавливаем rsi(он снова указывает на метку)

        cmp rax, 1          ; Если rax = 1, то строки равны
        je .success

        mov rsi, [rsi]      ; Указатель на следующий элемент
        cmp rsi, 0          ; Если rsi = 0, то мы прошлись по всем элементам
        je .wasted
        jmp .loop          

    .wasted:
        xor rax, rax        ; Элемент не был найден 
        ret

    .success:
        mov rax, rsi        ; Записываем в rax метку вхождения
        ret 
