%include "words.inc"
%include "lib.inc"
%define BUFF_SIZE 256
%define DQ_SIZE 8


extern find_word

global _start

section .rodata
not_found_string: db "Значение по данному ключу не найдено", 0
not_enough_string: db "Введённая строка больше чем 255 символов", 0


section .text

_start:
    sub rsp, BUFF_SIZE
    mov rdi, rsp
    mov rsi, BUFF_SIZE
    call read_word
    cmp rax, 0                      ; В rax лежит 0, если не получилось прочитать слово
    je .not_enough
                                    ; Если прочитали, то ищем слово в буфере
    mov rdi, rsp                    ; Записываем в rdi адрес буфера
    mov rsi, next_pointer           ; Записываем в rsi метку first
    call find_word
    add rsp, BUFF_SIZE
    cmp rax, 0                      
    je .not_found                   ; Если ключ равен нулю, то строка не была найдена

    add rax, DQ_SIZE                ; Смещаем указатель на значение ключа
    mov rdi, rax                    ; Кладём в rdi ключ
    call string_length              ; Вычисляет длину строки в rax
    add rdi, rax                    ; Переходим к нуль-терминатору 
    inc rdi
    call print_string               ; Выводим значение по ключу
    xor rdi, rdi
    call exit                


    .not_enough:
        mov rdi, not_enough_string
        jmp .print_error
        mov rdi, 1
        call exit

    .not_found:
        mov rdi, not_found_string
        jmp .print_error
        mov rdi, 1
        call exit

    
