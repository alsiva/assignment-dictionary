global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


section .text
  
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60                         ; sys_exit number
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
      cmp byte [rdi+rax], 0             ; Проверяем является ли символ нуль-терминатором
      je .end                           ; Если равен нулю то выходим
      inc rax                           ; Иначе увеличиваем rax(в нём хранится кол-во символов)
      jmp .count                        ; и переходим к следующему символу
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, 1
    jmp print_in_stream

print_error:
    mov rsi, 2

print_in_stream:
    call string_length
    push rdi
    push rsi
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    mov rdx, 1                          ; Длина строки, т.к это символ то 1              
    mov rax, 1                          ; sys_write number
    push rdi                        
    mov rsi, rsp                        ; Берём со стека указатель на символ              
    mov rdi, 1                          ; Stdout descriptor              
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA                        ; Записываем в rdi ASCII код переноса строки
    call print_char                     ; Используем print_char для вывода символа
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi                        
    mov r8, 10                          ; Делитель, при делении на 10 будем получать цифру числа
    push 0                              ; Потом, когда достанем со стека, будет обозначать конец числа

.loop:
    xor rdx, rdx 
    div r8                              ; В rax записывается частное, а в rdx остаток
    add rdx, 0x30                       ; Чтобы получить ASCII, нужно прибавить к числу 0x30
    push rdx
    cmp rax, 0                          
    ja .loop                            ; Если частное больше нуля, то значит остались цифры и продолжаем делить

.print:
    pop rdi
    cmp rdi, 0
    jz .end                             ; Если достали со стека 0, значит мы прошлись по всем символам
    call print_char                     ; Иначе это символ, и мы его печатаем
    jmp .print

.end:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print_positive                 ; Если число положительное, то печатаем также как беззнаковое

    push rdi                            
    mov rdi, '-'                        
    call print_char                     ; Иначе печатаем минус
    pop rdi
    neg rdi                             ; Меняем отрицательное на положительное, после чего печатаем как беззнаковое

.print_positive:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - указатель на первую строку
; rsi - указатель на вторую строку
string_equals:
.loop:
    mov al, byte[rdi]           ; записываем в al символ из первой строки
    mov dl, byte[rsi]           ; записываем в dl символ из второй строки
    cmp al, dl                  ; сравниваем два символа
    jne .not_equal               ; Если они не равны, то переходим в not_equal
    cmp al, 0                   ; Иначе они равны, и сравниваем символ с нуль-терминатором
    je  .equal                   ; Если это он, то это был последний символ и переходм в equal
    inc rdi                     ; Иначе это был не последний символ
    inc rsi                     ; и мы сдвигаем указатели обеих строк на следующий символ
    jmp .loop                   

.equal:
    mov rax, 1                  ; Возвращаем 1
    ret

.not_equal:
    mov rax, 0                  ; Возвращаем 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0                      ; Выделяем место в стеке
    mov rax, 0                  ; sys_read number
    mov rdi, 0                  ; stdin descriptor
    mov rdx, 1                  ; Длина строки, в данном случае символ поэтому 1
    mov rsi, rsp                ; Берём со стека указатель на символ
    syscall
    pop rax                     ; Достаём введённый символ из стека в rax                 
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

;rdi - адрес начала буфера
;rsi - размер буфера
;r8 - длина введённой строки
read_word:

push rdi                ; Записываем адрес начала буфера в стек 
push rdi                ; Записываем rdi и rsi в стек т.к read_char будет использовать их
push rsi

.first_whitespaces:
    
    call read_char          ; Читаем символ
    cmp rax, 0x20           ; Сравниваем символ с пробелом
    je .first_whitespaces                       
    cmp rax, 0x9            ; Сравниваем символ с табуляцией
    je .first_whitespaces
    cmp rax, 0xA            ; Сравниваем символ с переводом на новую строку
    je .first_whitespaces   ; Если этот символ один из пробельных, то мы его пропускаем и читаем следующий
    xor r8, r8              ; Очищаем r8(в нём будет хранится длина введённой строки)
    pop rsi                 ; Достаём rdi и rsi со стека 
    pop rdi   

.first_word:                ; Иначе мы переходим к чтению слова
    cmp rax, 0              ; Сравниваем символ с нуль-терминатором
    je .end                 ; Если это нуль-терминатор, то слово закончилось
    cmp rax, 0x20           ; Сравниваем символ с пробелом
    je .end                 ; Если это пробел, то слово закончилось                      
    cmp rax, 0x9            ; Сравниваем символ с табуляцией
    je .end                 ; Если это табуляция, то слово закончилось 
    cmp rax, 0xA            ; Сравниваем символ с переводом на новую строку
    je .end                 ; Если это перевод на нвоую строку, то слово закончилось

    cmp rsi, r8             ; Сравниваем размер буфера с количеством введённых символов
    je .buffer_small        ; Если они равны, то места нет(т.к мы хотим ввести как минимум ещё один символ)

    mov [rdi], rax          ; Записываем символ в буфер
    inc rdi                 ; Смещаем указатель буфера
    inc r8                  ; Увеличиваем длину строки на 1
    push rdi                ; Записываем rdi и rsi в стек т.к read_char будет использовать их
    push rsi
    call read_char          ; Читаем символ
    pop rsi                 ; Достаём rdi и rsi со стека 
    pop rdi
    jmp .first_word         ; Переходим к записи в буфер следующего символа

.buffer_small:
    pop rax                 ; Хоть мы и обнуляем rax, но это нужно чтобы восстановить указатель стека
    xor rax, rax            ; Записываем в rax 0
    ret              

.end:
    pop rax                 ; Достаём со стека адрес начала буфера 
    mov rdx, r8             ; Записываем в rdx длину строки
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

;rdi - указатель строки
;r8 - цифра
;r9 - длина числа
;r10 - множитель = 10 
parse_uint:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, 10
.read_digit:
    cmp byte[rdi], 0x30     ; Сравниваем ASCII код цифры с ASCII кодом 0
    jb .end                 ; Если код цифры меньше, то это не цифра
    cmp byte[rdi], 0x39     ; Сравниваем ASCII код цифры с ASCII кодом 9
    ja .end                 ; Если код цифры больше, то это не цифра
    mul r10                 ; Умножаем разряд на 10
    mov r8b, byte[rdi]      ; Записываем цифру в r8
    sub r8b, 0x30           ; Чтобы перевести ASCII код в цифру отнимаем 0x30
    add rax, r8             ; Добавляем цифру к числу
    inc rdi                 ; Смещаем указатель на следующий символ
    inc r9                  ; Увеличиваем длину числа на 1
    jmp .read_digit         ; Читаем следующий символ

.end:
    mov rdx, r9             ; Записываем длину числа в rdx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'      ; Сравниваем первый символ числа с минусом
    je .negative            ; Если это минус то переходим ко обработке отрицательного числа
    call parse_uint         ; Иначе обрабатываем как беззнаковое
    jmp .end 

.negative:
    inc rdi                 ; Смещаем указатель на первую цифру числа
    call parse_uint         ; Обрабатываем как беззнаковое
    cmp rdx, 0              ; Сравниваем длину строки с нулём
    je .end                 ; Число прочитать не удалось(длина равна 0)
    neg rax                 ; Меняем знак числа 
    inc rdx                 ; В длину числа также входит минус, поэтому +1

.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длина буфера
string_copy:
    push rdx                    
    call string_length          
    pop rdx
    inc rax                     ; Длину строки увеличиваем на 1, т.к туда должен поместиться нуль-терминатор
    cmp rax, rdx                ; Сравниваем длину строки с длиной буфера
    jg .buffer_small            ; Если буфер меньше строки, то возвращаем 0
    
    .loop:                      ; Иначе переносим строку в буфер
    mov bl, byte [rdi]          ; Кладём в bl символ по адресу строки
    mov byte [rsi], bl          ; Значение по указателю на буфер присваиваем символу лежащем в bl
    cmp rbx, 0                  ; Если символ который мы записали нуль-терминатор
    je .end                     ; то мы записали всё сроку
    inc rdi                     ; Иначе смещаем указатель строки на следующий символ
    inc rsi                     ; и смещаем указатель буфера на следующий символ
    jmp .loop                   ; Повторяем цикл

    .buffer_small:
    xor rax, rax
    
    .end:
    ret
